package com.waybank.controllers;

import com.waybank.entities.Cliente;
import com.waybank.entities.Conta;
import com.waybank.services.ClienteService;
import com.waybank.services.ContaService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/banking")
public class ClienteController {
    
    private ContaService contaService;
    private ClienteService clienteService;

    public ClienteController(ContaService contaService, ClienteService clienteService) {
        this.contaService = contaService;
        this.clienteService = clienteService;
    }
    
    
    @GetMapping("/listar-clientes")
    public String ListarClientes()
    {
        return "/waybank/lists/listarClientes";
    }
    
    @GetMapping("/cadastrar-cliente")
    public String CadastrarCliente(Model model)
    {
        model.addAttribute("cliente", new Cliente());
        model.addAttribute("conta", new Conta());
        
        return "/waybank/add/addCliente";
    }
    
    @PostMapping("/salvar-cliente")
    public String Salvar(Cliente cliente)
    {
        System.out.println(cliente);
        
        clienteService.Salvar(cliente);
        
        return "redirect:/banking/listar-clientes";
    }
    
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.waybank.controllers;

import com.waybank.services.ClienteService;
import com.waybank.services.ContaService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/banking")
public class WebBank {
    
    private ContaService contaService;
    private ClienteService clienteService;

    public WebBank(ContaService contaService, ClienteService clienteService) {
        this.contaService = contaService;
        this.clienteService = clienteService;
    }
    
    @RequestMapping("/dashboard")
    public String home()
    {
        return "waybank/dash";
    }
    
}

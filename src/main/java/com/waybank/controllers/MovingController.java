/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.waybank.controllers;

import com.waybank.entities.Cliente;
import com.waybank.entities.Conta;
import com.waybank.entities.Moving;
import com.waybank.services.ClienteService;
import com.waybank.services.ContaService;
import com.waybank.services.MovingService;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

/**
 *
 * @author juniorrodrigues
 */
public class MovingController {
    
    private ContaService contaService;
    private ClienteService clienteService;
    private MovingService movingService;

    public MovingController(ContaService contaService, ClienteService clienteService, MovingService movingService) {
        this.contaService = contaService;
        this.clienteService = clienteService;
        this.movingService = movingService;
    }
    
    
    @GetMapping("/listar-movimentacoes")
    public String ListarClientes()
    {
        return "/waybank/lists/listarMovimentacoes";
    }
    
    @GetMapping("/cadastrar-transacao")
    public String CadastrarCliente(Model model)
    {
        model.addAttribute("transacao", new Moving());
        model.addAttribute("conta", new Conta());
        
        return "/waybank/add/addTransacao";
    }
    
    @PostMapping("/salvar-cliente")
    public String Salvar(Moving movimentacao)
    {
        System.out.println(movimentacao);
        
        movingService.Salvar(movimentacao);
        
        return "redirect:/banking/listar-movimentacoes";
    }
}

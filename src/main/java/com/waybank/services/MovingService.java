
package com.waybank.services;

import com.waybank.entities.Conta;
import com.waybank.entities.Moving;
import java.util.List;
import org.springframework.stereotype.Service;

@Service
public interface MovingService {
    
    public void Salvar(Moving conta);
    public Moving buscarPorId(Long id);
    public Moving buscarPorMovimentacao(String conta);
    public List<Moving> listarTodas();

}

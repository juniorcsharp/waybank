package com.waybank.services;

import com.waybank.entities.Cliente;
import java.util.List;
import org.springframework.stereotype.Service;

@Service
public interface ClienteService {
    
    public void Salvar(Cliente cliente);
    public Cliente buscarPorId(Long id);
    public List<Cliente> ListarTodos();
    public void RemoverPorId(Long id);
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.waybank.services;

import com.waybank.entities.Cliente;
import com.waybank.repositories.ClienteRepository;
import com.waybank.repositories.ContaRepository;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import org.springframework.stereotype.Service;

@Service
public class ClienteServiceImpl implements ClienteService{
    
    private ClienteRepository clienteRepository;
    private ContaRepository contaRepository;

    public ClienteServiceImpl(ClienteRepository clienteRepository, ContaRepository contaRepository) {
        this.clienteRepository = clienteRepository;
        this.contaRepository = contaRepository;
    }
    
    

    @Override
    public void Salvar(Cliente cliente) {
        this.clienteRepository.save(cliente);
       
    }
    

    @Override
    public Cliente buscarPorId(Long id) {
        Optional<Cliente> o = clienteRepository.findById(id);
        
        return o.get();
    }

    @Override
    public List<Cliente> ListarTodos() {
        List<Cliente> clientes = new ArrayList<>();
        Iterator<Cliente> iterator = this.clienteRepository.findAll().iterator();
        
        while (iterator.hasNext()) {
            clientes.add(iterator.next());
            
        }
        
        return clientes;
    }

    @Override
    public void RemoverPorId(Long id) {
        this.clienteRepository.deleteById(id);
    }
    
    
    
}

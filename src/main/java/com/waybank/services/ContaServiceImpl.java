package com.waybank.services;

import com.waybank.entities.Cliente;
import com.waybank.entities.Conta;
import com.waybank.repositories.ContaRepository;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import org.springframework.stereotype.Service;

@Service
public class ContaServiceImpl implements ContaService{
    
    private ContaRepository contaRepository;
    

    public ContaServiceImpl(ContaRepository contaRepository) {
        this.contaRepository = contaRepository;
    }
    

    @Override
    public void Salvar(Conta conta) {
        this.contaRepository.save(conta);
    }

    @Override
    public Conta buscarPorId(Long id) {
        Optional<Conta> o = contaRepository.findById(id);
        
        return o.get();
    }

    @Override
    public Conta buscarPorConta(String conta) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public List<Conta> listarTodas() {
        List<Conta> contas = new ArrayList<>();
        Iterator<Conta> iterator = this.contaRepository.findAll().iterator();
        
        while (iterator.hasNext()) {
            contas.add(iterator.next());
            
        }
        
        return contas;
    }

    @Override
    public void RemoverPorId(Long id) {
        this.contaRepository.deleteById(id);
    }
    
}

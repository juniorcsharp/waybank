/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.waybank.services;

import com.waybank.entities.Conta;
import com.waybank.entities.Moving;
import com.waybank.repositories.CategoriaRepository;
import com.waybank.repositories.MovingRepository;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import org.springframework.data.repository.CrudRepository;

/**
 *
 * @author juniorrodrigues
 */
public class MovingServiceimpl implements MovingService {

    private MovingRepository movingRepository;
    private CategoriaRepository categoriaRepository;

    public MovingServiceimpl(MovingRepository movingRepository, CategoriaRepository categoriaRepository) {
        this.movingRepository = movingRepository;
        this.categoriaRepository = categoriaRepository;
    }

    @Override
    public void Salvar(Moving conta) {
        this.movingRepository.save(conta);
    }

    @Override
    public Moving buscarPorId(Long id) {
        Optional<Moving> o = movingRepository.findById(id);

        return o.get();
    }

    @Override
    public List<Moving> listarTodas() {
        List<Moving> movimentacoes = new ArrayList<>();
        Iterator<Moving> iterator = this.movingRepository.findAll().iterator();

        while (iterator.hasNext()) {
            movimentacoes.add(iterator.next());

        }

        return movimentacoes;
    }

    @Override
    public Moving buscarPorMovimentacao(String conta) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}

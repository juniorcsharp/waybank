/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.waybank.services;

import com.waybank.entities.Conta;
import java.util.List;
import org.springframework.stereotype.Service;

@Service
public interface ContaService {
    
    public void Salvar(Conta conta);
    public Conta buscarPorId(Long id);
    public Conta buscarPorConta(String conta);
    public List<Conta> listarTodas();
    public void RemoverPorId(Long id);
    
}

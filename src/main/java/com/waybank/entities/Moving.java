package com.waybank.entities;

import com.waybank.entities.Categoria;
import com.waybank.entities.Conta;
import java.util.Date;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Temporal;

@Entity
public class Moving {
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int conta_origem;
    private int conta_destino;
    
    private double valor;
    
    private Date created;
    private Date updated;
    
    @PrePersist
    protected void onCreate() {
      created = new Date();
    }

    @PreUpdate
    protected void onUpdate() {
      updated = new Date();
    }
    
    private double taxa;
    
    @OneToMany
    @JoinColumn(name = "conta_id")
    private List<Conta> contas;
    
    @ManyToMany
    @JoinColumn(name = "categoria_id")
    private List<Categoria> categorias;

    public int getConta_origem() {
        return conta_origem;
    }

    public void setConta_origem(int conta_origem) {
        this.conta_origem = conta_origem;
    }

    public int getConta_destino() {
        return conta_destino;
    }

    public void setConta_destino(int conta_destino) {
        this.conta_destino = conta_destino;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getUpdated() {
        return updated;
    }

    public void setUpdated(Date updated) {
        this.updated = updated;
    }

    public double getTaxa() {
        return taxa;
    }

    public void setTaxa(double taxa) {
        this.taxa = taxa;
    }

    public List<Conta> getContas() {
        return contas;
    }

    public void setContas(List<Conta> contas) {
        this.contas = contas;
    }

    public List<Categoria> getCategorias() {
        return categorias;
    }

    public void setCategorias(List<Categoria> categorias) {
        this.categorias = categorias;
    }

    @Override
    public String toString() {
        return "Moving{" + "conta_origem=" + conta_origem + ", conta_destino=" + conta_destino + ", valor=" + valor + ", created=" + created + ", updated=" + updated + ", taxa=" + taxa + ", contas=" + contas + ", categorias=" + categorias + '}';
    }
    
    
    
}

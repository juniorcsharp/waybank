package com.waybank.repositories;

import com.waybank.entities.Conta;
import org.springframework.data.repository.CrudRepository;

public interface ContaRepository extends CrudRepository<Conta, Long>{
    
}

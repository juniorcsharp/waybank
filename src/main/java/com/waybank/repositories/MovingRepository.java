package com.waybank.repositories;

import com.waybank.entities.Moving;
import org.springframework.data.repository.CrudRepository;

public interface MovingRepository extends CrudRepository<Moving, Long>{
    
}

package com.waybank;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WayBanckApplication {

	public static void main(String[] args) {
		SpringApplication.run(WayBanckApplication.class, args);
	}

}
